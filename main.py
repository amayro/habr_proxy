import requests
from http.server import BaseHTTPRequestHandler, HTTPServer
from handlers import add_label, replace_url, extract_body_html, attach_body_modify


class HttpProcessor(BaseHTTPRequestHandler):

    @staticmethod
    def modify_body(content):
        """Производит модификацию body"""

        modify_list = [
            add_label,
            replace_url
        ]
        body = extract_body_html(content)

        for modify in modify_list:
            body = modify(body)

        return body

    def do_GET(self):
        """GET запрос"""

        url_path = self.path
        request = requests.get(f'https://habr.com{url_path}')
        encoding_name = request.encoding
        self.send_response(200)
        self.send_header('content-type', request.headers['Content-Type'])
        self.end_headers()

        if request.headers['Content-Type'].startswith('text/html'):
            content = request.content.decode(encoding=encoding_name)
            body_modify = self.modify_body(content)
            content_modify = attach_body_modify(content, body_modify)

            response = content_modify.encode(encoding=encoding_name)
        else:
            response = request.content

        self.wfile.write(response)


server = HTTPServer(("localhost", 8000), HttpProcessor)
server.serve_forever()