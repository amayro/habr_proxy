import pytest
from handlers import add_label


@pytest.fixture
def label():
    return '™'


@pytest.fixture
def text_variants(label):
    """
    Список вариантов текста в виде кортежа, где
    1 элемент - это исходный текст,
    2 элемент - ожидаемый текст
    """

    return [
        (
            """<body class='example'>
            Сейчас на фоне <a href="http://habrahabr.ru/company/dsec/blog/258457/">уязвимости Logjam</a>
            все в индустрии в очередной раз обсуждают проблемы и особенности TLS.\n
            Я хочу воспользоваться этой возможностью, чтобы поговорить об одной из них,
            а именно — о настройке ciphersiutes.
            </body>""",

            f"""<body class='example'>
            Сейчас{label} на фоне <a href="http://habrahabr.ru/company/dsec/blog/258457/">уязвимости Logjam{label}</a>
            все в индустрии в очередной раз обсуждают проблемы и особенности TLS.\n
            Я хочу воспользоваться этой возможностью, чтобы поговорить об одной из них,
            а именно{label} — о настройке ciphersiutes.
            </body>"""
        ),
        (
            """<ul>
            <li>Python 3.5+</li>
            <li>страницы должны отображаться и работать полностью корректно, в точности так,
            как и оригинальные (за исключением модифицированного текста);</li>
            <li>при навигации по ссылкам, которые ведут на другие страницы хабра, браузер
            должен оставаться на адресе вашего прокси;</li>
            <li>можно использовать любые общедоступные библиотеки, которые сочтёте нужным;</li>
            <li>чем меньше кода, тем лучше. PEP8 — обязательно;</li>
            <li>если в условиях вам не хватает каких-то данных, опирайтесь на здравый смысл.</li>
            </ul>""",

            f"""<ul>
            <li>Python{label} 3.5+</li>
            <li>страницы должны{label} отображаться и работать полностью корректно, в точности так,
            как и оригинальные (за исключением модифицированного текста{label});</li>
            <li>при навигации по ссылкам, которые ведут на другие{label} страницы хабра, браузер
            должен{label} оставаться на адресе{label} вашего{label} прокси{label};</li>
            <li>можно использовать любые общедоступные библиотеки, которые сочтёте нужным{label};</li>
            <li>чем меньше{label} кода, тем лучше. PEP8 — обязательно;</li>
            <li>если в условиях вам не хватает каких-то данных{label}, опирайтесь на здравый смысл.</li>
            </ul>"""
        ),
        (
            """<body class='Logjam search'>Сейчас! Нет Logjam именно.</body>""",

            f"""<body class='Logjam search'>Сейчас{label}! Нет Logjam{label} именно{label}.</body>"""
        ),
        (
            """<body>html entities &thinsp;</body>""",

            """<body>html entities &thinsp;</body>""",
        ),
        (
            """<script type="text/javascript">Сейчас! Нет Logjam именно.</script>""",

            """<script type="text/javascript">Сейчас! Нет Logjam именно.</script>""",
        ),

    ]


def test_add_label(text_variants, label):
    """Тест на валидность текста."""

    for text_original, text_expected in text_variants:
        assert add_label(text_original, label=label) == text_expected


