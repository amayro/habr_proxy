import pytest
from handlers import replace_url


@pytest.fixture
def text_variants():
    """
    Список вариантов текста с url в виде кортежа, где
    1 элемент - это исходный текст,
    2 элемент - ожидаемый текст
    """

    return [
        (
            '<a href="http://habrahabr.ru/company/dsec/blog/258457/">уязвимости Logjam</a>',
            '<a href="/company/dsec/blog/258457/">уязвимости Logjam</a>'
        ),
        (
            '<a href="https://habr.com/company/dsec/blog/258457/">уязвимости Logjam</a>',
            '<a href="/company/dsec/blog/258457/">уязвимости Logjam</a>'
        ),
        (
            'habrahabr.ru и habr.com тестирует уязвимости Logjam',
            'habrahabr.ru и habr.com тестирует уязвимости Logjam',
        ),
    ]


def test_add_label(text_variants):
    """Тест на валидность текста."""

    for text_original, text_expected in text_variants:
        assert replace_url(text_original) == text_expected
