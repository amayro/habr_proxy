import pytest
from handlers import attach_body_modify


@pytest.fixture
def template_html():
    return """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" 
              "http://www.w3.org/TR/html4/strict.dtd">
            <html>
             <head>
               <title>!DOCTYPE</title>
               <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
             </head>
             <body>
              <p>Разум — это Будда</p>
             </body> 
            </html>"""


@pytest.fixture
def body_html():
    return """<body>
              <p>Разум — это Будда, а прекращение умозрительного мышления — это путь. 
              Перестав мыслить понятиями и размышлять о путях существования и небытия, 
              о душе и плоти, о пассивном и активном и о других подобных вещах, 
              начинаешь осознавать, что разум — это Будда, 
              что Будда — это сущность разума, 
              и что разум подобен бесконечности.</p>
             </body>"""


@pytest.fixture
def expected_html():
    return """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" 
              "http://www.w3.org/TR/html4/strict.dtd">
            <html>
             <head>
               <title>!DOCTYPE</title>
               <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
             </head>
             <body>
              <p>Разум — это Будда, а прекращение умозрительного мышления — это путь. 
              Перестав мыслить понятиями и размышлять о путях существования и небытия, 
              о душе и плоти, о пассивном и активном и о других подобных вещах, 
              начинаешь осознавать, что разум — это Будда, 
              что Будда — это сущность разума, 
              и что разум подобен бесконечности.</p>
             </body> 
            </html>"""


def test_extract_body_html(template_html, body_html, expected_html):
    assert attach_body_modify(template_html, body_html) == expected_html
