import re


def add_label(text: str, count_letter: int = 6, label: str = '™') -> str:
    """
    Заменяет исходный html на html с пометкой

    **Args**:

    ``text`` - исходная страница html

    ``count_letter`` - из скольких букв должно состоять слово, которому необходимо добавить label

    ``label`` - пометка, которую необходимо вставить
    """

    pattern = r'(<(?!script).*>[^<&]*)(\b\w{' + str(count_letter) + r'}\b)([^' + label + '])'
    while re.findall(pattern, text):
        text = re.sub(pattern, r'\1\2' + label + r'\3', text)

    return text


def replace_url(text: str) -> str:
    """Заменяет url хабра в исходном тексте"""

    return re.sub('(href=")(http|https)://(habrahabr.ru|habr.com)', r'\1', text)


def extract_body_html(str_html: str) -> str:
    """Извлекает body из html"""

    return re.findall('<body.+</body>', str_html, flags=re.S)[0]


def attach_body_modify(str_html: str, body_modify: str) -> str:
    """Прикрепляет измененный body к html"""

    body_original = extract_body_html(str_html)
    return str_html.replace(body_original, body_modify)
